<?php

class genereteDiscountCodes
{
    private $length;
    private $items;

    public function __construct($items, $length)
    {
        $this->items = $items;
        $this->length = $length;

    }

    public function getSingleCode($length)
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    public function getCollectionCode()
    {

    }

    public function saveCollectionToFile() {
        $file = 'discountCodes.txt';
        $current = file_get_contents($file);

        $current .= $this->getSingleCode($_POST['lengthCode'])."\n";

        return file_put_contents($file, $current);
    }

}

$genereteCode = new genereteDiscountCodes();

print_r($genereteCode->getCollectionCode());

?>
